
package com.transcendsys.platform.server.workflow;

import java.io.FileInputStream;
import java.util.*;

import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import org.apache.log4j.*;
import org.apache.log4j.Level;
import org.springframework.beans.BeansException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

public class WorkflowExecutorBean extends BaseAccessorBean implements SessionBean {

  static final PlatformLogger LOG = PlatformLogger.get(WorkflowExecutorBean.class);
  private static int runningThreadsCount=-1;
  private static TreeMap<String,String> threadsTreeMap = new TreeMap<String,String>();
  private static Map<String,String> threadToTimeMap = Collections.synchronizedMap(threadsTreeMap);
  
  public static int getRunningThreadsCount() {
    return runningThreadsCount;
  }
  
  public static void setRunningThreadsCount(int num) {
    runningThreadsCount = num;
  }
  
  public static String getFormattedLog(String toPad) {
    if(runningThreadsCount >= 1)
      return String.format("%"+runningThreadsCount+"s", "").replace(' ', '+') + toPad;
    else 
      return toPad;
  }
  
  /**
   * @ejb.interface-method view-type="local"
   * This method loops thru the workflow activities in the specified sequence and executes them one by one.
   * This is the API to be used in production mode
   * 
   * @param workflow the workflow to execute
   * @param dvceCtx the DVCE context
   * @throws com.transcendsys.platform.server.workflow.WorkflowBaseException workflow exception
   */
  public void executeWorkflow(
    com.transcendsys.platform.server.workflow.Workflow workflow,
    com.transcendsys.platform.base.context.DvceContext dvceCtx)
    throws com.transcendsys.platform.server.workflow.WorkflowBaseException {

    //pass null for WorkflowValidator
    // executeWorkflow(workflow, null, dvceCtx); 
    
    // if(1 == 1) return; // Disable threading
	
    // ctx.get
    // TODO REMOVE New Thread for WF Execution
    class ExecutionThreadTask implements Runnable {
      private PlatformLogger LOG = PlatformLogger.get(ExecutionThreadTask.class);
      private com.transcendsys.platform.server.workflow.Workflow workflow = null;
      private com.transcendsys.platform.base.context.DvceContext dvceCtx = null;
      private boolean threadContextInheritable = false;
      long startTime = 0;
      long endTime = 0;
      
      public ExecutionThreadTask(com.transcendsys.platform.server.workflow.Workflow workflow,
        com.transcendsys.platform.base.context.DvceContext dvceCtx) {
        
        this.workflow = workflow;
        this.dvceCtx = dvceCtx;
      }
      
      @Override
      public void run() {
        synchronized(this){
    			try {
    			  ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
    				LocaleContextHolder.setLocale(Locale.getDefault(), this.threadContextInheritable);
    				RequestContextHolder.setRequestAttributes(attributes, this.threadContextInheritable);
    				//LOG.info("Bound request context to thread: " + request);
    
    				try {
    					startTime = System.currentTimeMillis();
    					LOG.info(getFormattedLog(workflow.getWfType().getName() + " -> WF ExecutionThread started..."));
    
    					try {
    						executeWorkflow(workflow, null, dvceCtx);
    					} catch(Exception e) {
    						e.printStackTrace();
    					}
    					
    					LOG.info(getFormattedLog(workflow.getWfType().getName() + " --> WF ExecutionThread completed"));
    					endTime = System.currentTimeMillis();
    					
    					//System.out.println(getRunningThreadsCount());
						threadToTimeMap.put(workflow.getWfType().getName(),getPaddingPlusRunTime().replace("_", "."));
    					printCompletionMessage();
    					
    					notify(); // Release lock	
    					
    				} finally {
    					LocaleContextHolder.resetLocaleContext();
    					RequestContextHolder.resetRequestAttributes();
    					if(attributes != null) attributes.requestCompleted();
    					//LOG.info("Cleared thread-bound request context: " + request);
    				}
    			} catch(IllegalStateException e) {
    				e.printStackTrace();
    			} catch(Exception e) {
    				e.printStackTrace();
    			}
        }
      }
     
      public String getTotalRunTime() {
        return ((endTime - startTime) + "ms");
      }
      
      public String getPaddingPlusRunTime() {
        String padding = String.format("%"+ (100-(workflow.getWfType().getName() + " ---> Completed - ").length()-getRunningThreadsCount()) +"s", "").replace(' ', '_');
        return padding + getTotalRunTime();
      }
      
      public void printCompletionMessage() {
        LOG.info(getFormattedLog(workflow.getWfType().getName() + " ---> Completed - " + " " + getPaddingPlusRunTime()));
        
        if(getRunningThreadsCount() == 0 && !threadToTimeMap.isEmpty() && threadToTimeMap.size() > 1) {
          LOG.info("##########  Thread to Time Map: #########");
          
          //Iterator<String> keyIter = ((TreeMap) threadToTimeMap).descendingKeySet().iterator();
          //Iterator<String> keyIter = threadToTimeMap.keySet().iterator();
          Iterator<String> keyIter = threadsTreeMap.descendingKeySet().iterator();
          for(int i=0; i < threadToTimeMap.size() && keyIter.hasNext(); i++) {
            String tname = keyIter.next();
            String ttime = threadToTimeMap.get(tname);
            LOG.info(i + ". " + tname + " - " + ttime);
          }
          threadToTimeMap.clear();
          LOG.info("##########################################");
        } else if (getRunningThreadsCount() == 0) {
          threadToTimeMap.clear();
        } 
      }
    }
    
    String threadName = "WEThread-" + ((workflow.getActionTypeDef() != null) ? "Action-" + workflow.getActionTypeDef().getName()
      + "-" + workflow.getWfType().getModelType().getValue()
    : "WF-" + workflow.getWfType().getName() + "-" + workflow.getWfType().getModelType().getValue());
    
    Thread executionThread = new Thread(new ExecutionThreadTask(workflow,dvceCtx));
    executionThread.setName(threadName);
    executionThread.start();
    
    synchronized(executionThread){
      try{
          setRunningThreadsCount(getRunningThreadsCount()+1);
          LOG.info(getFormattedLog(workflow.getWfType().getName() + " > Started and waiting for it to complete..."));
          executionThread.wait(); // Get lock
      } catch(InterruptedException e){
        e.printStackTrace();
      } catch(Exception e) {
        e.printStackTrace();
      }
      setRunningThreadsCount(getRunningThreadsCount()-1);
//      if(threadName.equals("ChrononWFThread-Action-RPL.CreateApproved")) {
//        Exception e = new Exception("ChrononWFThread-Action-RPL.CreateApproved Exec");
//        e.printStackTrace();
//      }
    }
	
    
  }
  
}
